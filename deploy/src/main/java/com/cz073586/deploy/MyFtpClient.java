/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cz073586.deploy;

import org.apache.camel.main.Main;

/**
 * Main class that can upload files to an existing FTP server.
 */
public final class MyFtpClient
{
	
	private MyFtpClient()
	{
	}
	
	public static void main(String[] args) throws Exception
	{
		System.out.println("-----------------------------------------------");
		System.out.println("欢迎使用零点代码发布工具. \n\n by:binghen");
		System.out.println("-----------------------------------------------");
		Main main = new Main();
		main.addRouteBuilder(new MyFtpClientRouteBuilder());
		main.enableHangupSupport();
		main.run();
	}
	
}