/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cz073586.deploy;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.properties.PropertiesComponent;

/**
 * Client route
 */
public class MyFtpClientRouteBuilder extends RouteBuilder
{
	
	@Override
	public void configure() throws Exception
	{
		// configure properties component
		PropertiesComponent pc = getContext().getComponent("properties",
				PropertiesComponent.class);
		pc.setLocation("classpath:ftp.properties");
		
		getContext().getShutdownStrategy().setTimeout(10);
		from("file:{{client.path}}")
				.log("Uploading file ${file:name}")
				.to("ftp://{{ftp.host}}:{{ftp.port}}/?username={{ftp.userName}}&password={{ftp.password}}")
				.log("[零点]文件处理--> ${file:name} 已经完成...");
		
	}
}